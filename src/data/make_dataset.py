# -*- coding: utf-8 -*-
import os
import pickle

import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
from sklearn import datasets
from sklearn.model_selection import train_test_split


def load_and_split():
    X, y = datasets.load_iris(return_X_y=True)
    X_pretrain, X_test, y_pretrain, y_test = train_test_split(X, y, test_size=0.2)
    X_train, X_dev, y_train, y_dev = train_test_split(X_pretrain, y_pretrain, test_size=0.2)
    return X_train, X_dev, X_test, y_train, y_dev, y_test


def save_data(path, X_train, X_dev, X_test, y_train, y_dev, y_test):
    def do_pickle(obj, path):
        with open(path, "wb") as fp:
            pickle.dump(obj, fp)

    do_pickle(X_train, f"{path}/X_train.pickle")
    do_pickle(X_dev, f"{path}/X_dev.pickle")
    do_pickle(X_test, f"{path}/X_test.pickle")
    do_pickle(y_train, f"{path}/y_train.pickle")
    do_pickle(y_dev, f"{path}/y_dev.pickle")
    do_pickle(y_test, f"{path}/y_test.pickle")


@click.command()
@click.option('--run_name', type=click.STRING)
@click.option('--output_filepath', type=click.Path())
def main(run_name, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    path = f"{output_filepath}/{run_name}"

    os.makedirs(f"{path}", exist_ok=True)
    X_train, X_dev, X_test, y_train, y_dev, y_test = load_and_split()
    save_data(path, X_train, X_dev, X_test, y_train, y_dev, y_test)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
