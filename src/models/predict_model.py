import argparse
import pickle

import click
import pandas
from sklearn.metrics import classification_report


def load_data(data_path):
    def unpickle(path):
        with open(path, "rb") as fp:
            result = pickle.load(fp)
        return result
    
    X_train = unpickle(f"{data_path}/X_train.pickle")
    X_dev = unpickle(f"{data_path}/X_dev.pickle")
    X_test = unpickle(f"{data_path}/X_test.pickle")
    y_train = unpickle(f"{data_path}/y_train.pickle")
    y_dev = unpickle(f"{data_path}/y_dev.pickle")
    y_test = unpickle(f"{data_path}/y_test.pickle")
    return X_train, X_dev, X_test, y_train, y_dev, y_test


def load_model(models_path):
    with open(f"{models_path}/model.pickle", "rb") as fp:
        model = pickle.load(fp)
    return model


def predict_and_save(model, X, y, path):
    y_pred = model.predict(X)
    report = classification_report(y, y_pred, output_dict=True)
    df = pandas.DataFrame(report).transpose()
    df.to_csv(path, sep="\t")


@click.command()
@click.option('--run_name', type=click.STRING)
@click.option('--data_path', type=click.Path())
@click.option('--models_path', type=click.Path())
@click.option('--results_path', type=click.Path())
@click.option('--log_path', type=click.Path())
def main(run_name, data_path, models_path, results_path, log_path):
    data_path = f"{data_path}/{run_name}"
    models_path = f"{models_path}/{run_name}"
    results_path = f"{results_path}/{run_name}"
    log_path = f"{log_path}/{run_name}"

    X_train, X_dev, X_test, y_train, y_dev, y_test = load_data(data_path)
    model = load_model(models_path)
    predict_and_save(model, X_train, y_train, f"{results_path}/train_report.csv")
    predict_and_save(model, X_dev, y_dev, f"{results_path}/dev_report.csv")
    predict_and_save(model, X_test, y_test, f"{results_path}/test_report.csv")


if __name__ == "__main__":
    main()