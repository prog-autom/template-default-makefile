import os

import click
import pandas
import pickle
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import classification_report
import tensorboard_logger


def loadData(data_path):
    def loadFromPickle(path):
        with open(path, "rb") as fp:
            result = pickle.load(fp)
        return result

    X_train = loadFromPickle(f"{data_path}/X_train.pickle")
    X_dev = loadFromPickle(f"{data_path}/X_dev.pickle")
    y_train = loadFromPickle(f"{data_path}/y_train.pickle")
    y_dev = loadFromPickle(f"{data_path}/y_dev.pickle")
    return X_train, X_dev, y_train, y_dev


def loadModel(models_path):
    try:
        with open(f"{models_path}/model.pickle", "rb") as fp:
            model = pickle.load(fp)
    except FileNotFoundError:
        model = SGDClassifier()
    return model


def saveModel(models_path, model):
    with open(f"{models_path}/model.pickle", "wb") as fp:
        pickle.dump(model, fp)


def trainModel(log_path, model, numOfEpochs, X_train, y_train):
    tensorboard_logger.configure(f"{log_path}/log_tb/")
    for i in range(1, numOfEpochs):
        model.partial_fit(X_train, y_train, list(set(y_train)))
        y_pred = model.predict(X_train)
        report = classification_report(y_train, y_pred, output_dict=True)
        tensorboard_logger.log_value("f1-score", report["macro avg"]["f1-score"], i)
        if i % 10 == 0:
            with open(f"{log_path}/log/train_log_{i}", "w") as outp:
                report_raw = classification_report(y_train, y_pred)
                outp.write(report_raw)
    return model


def saveResult(results_path, model, X_dev, y_dev):
    y_pred = model.predict(X_dev)
    report = classification_report(y_dev, y_pred, output_dict=True)
    df = pandas.DataFrame(report).transpose()
    df.to_csv(f"{results_path}/report.csv", sep="\t")

@click.command()
@click.option("--run_name", type=click.STRING)
@click.option("--num_epochs", type=click.INT)
@click.option('--data_path', type=click.Path())
@click.option('--models_path', type=click.Path())
@click.option('--results_path', type=click.Path())
@click.option('--log_path', type=click.Path())
def main(run_name, num_epochs, data_path, models_path, results_path, log_path):

    data_path = f"{data_path}/{run_name}"
    models_path = f"{models_path}/{run_name}"
    results_path = f"{results_path}/{run_name}"
    log_path = f"{log_path}/{run_name}"

    os.makedirs(f"{models_path}", exist_ok=True)
    os.makedirs(f"{log_path}/log", exist_ok=True)
    os.makedirs(f"{log_path}/log_tb", exist_ok=True)
    os.makedirs(f"{results_path}", exist_ok=True)

    X_train, X_dev, y_train, y_dev = loadData(data_path)
    model = loadModel(models_path)
    model = trainModel(log_path, model, num_epochs, X_train, y_train)
    saveModel(models_path, model)
    saveResult(results_path, model, X_dev, y_dev)


if __name__ == "__main__":
    main()
